import { Router } from "@angular/router";
import { AuthService } from "./../_services/auth.service";
import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
} from "@angular/router";
import { Observable } from "rxjs";
import { AlertifyService } from "../alertify.service";

@Injectable({
  providedIn: "root",
})
export class AuthGuard implements CanActivate {
  /**
   *
   */
  constructor(
    private authService: AuthService,
    private router: Router,
    private alrtify: AlertifyService
  ) {}

  canActivate(): boolean {
    if (this.authService.loggedIn()) {
    }
    return true;
  }
}
