using System.Threading.Tasks;
using Jaws.API.Models;

namespace Jaws.API.Data
{
    public interface IAuthRepository
    {
        Task<User> Register(User user, string password);
        Task<User> Login(string username, string password);
        Task<bool> UserExists(string username);
    }
}