using System.ComponentModel.DataAnnotations;

namespace Jaws.API.DTO
{
    public class UserForRegisterDTO
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [StringLength(8,MinimumLength = 4,ErrorMessage = "You must specify password with length between 4 and 8 characters")]
        public string Password { get; set; }
    }
}